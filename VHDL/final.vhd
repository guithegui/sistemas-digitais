--**********************************************************--
--																				--	
--   		UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL  	      --
--							ENGENHARIA ELETRICA						   --
--																				--
--   ALUNOS: Felipe Signor (209077)             			   --
--			    Guilherme Rosa (206745)							   --			
--																			   --	
--   TITULO: CONTROLE DE POTÊNCIA UTILIZANDO CODIFICAÇÃO 	--
--           SIGMA-DELTA EM VHDL 									--
--																				--
--**********************************************************--

-- declaracao das bibliotecas
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.math_real.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


----------------------------------------------------------------

-------------------------------------------------------------
 
entity PWM is  -- inicio da descicao da entidade
  port (
   clk : in std_logic;  -- sinal de entrada
	rst : in std_logic;-- sinal de entrada
	PWM_out : out std_logic; -- sinal de saida do codificador
	displayA : out bit_vector (6 downto 0);-- sinal de saida display
	displayB : out bit_vector (6 downto 0);-- sinal de saida display
	displayC : out bit_vector (6 downto 0);-- sinal de saida display
	displayD : out bit_vector (6 downto 0);-- sinal de saida display
   PWM_test : out std_logic_vector (7 downto 0) := "00000000";
	 -- sinais de teste da entrada do codificado
	mais   :in std_logic;-- sinal de entrada botao de controle
	menos  :in  std_logic;-- sinal de entrada botao de controle
	rampa_c_set:in std_logic;-- sinal de entrada modo automatico
	rampa_mais_menos_set:in std_logic-- sinal de entrada modo da rampa
	
  );
end PWM;

architecture PWM_funcionamento of PWM is  -- inicio da descicao do funcionamento

 -- 'signal' sao sinais internos
  signal display1 : Integer range 0 to 9 := 0;
  signal display2 : Integer range 0 to 9 := 0;
  signal display3 : Integer range 0 to 9 := 0;
  signal display4 : Integer range 0 to 9 := 0;

  signal  PWM_Somador : std_logic_vector(8 downto 0);
  signal  diff : std_logic_vector (8 downto 0);
  signal  ddc : std_logic_vector (7 downto 0);
  signal aux_mais : std_logic;
  shared variable auxj: std_logic_vector (7 downto 0) := "00000000";
  signal  aux, auxm,auxt,auxc : std_logic_vector (7 downto 0) := "00000000"; 	
  signal PWM_in : std_logic_vector (7 downto 0):= x"00";
  signal contador: integer range 0 to 5000000 := 0;--  
  
begin
display: process (pwm_in)  -- processo do display
begin
-- Valores da entrada do pwm a serem demonstrados no display de 7 segmentos
	case pwm_in is    
when "00000000" =>  display1 <= 0; display2 <= 0; display3 <= 0; display4 <= 0;
when "00000001" =>  display1 <= 9; display2 <= 3; display3 <= 0; display4 <= 0;
when "00000010" =>  display1 <= 8; display2 <= 7; display3 <= 0; display4 <= 0;
when "00000011" =>  display1 <= 7; display2 <= 1; display3 <= 1; display4 <= 0;
when "00000100" =>  display1 <= 6; display2 <= 5; display3 <= 1; display4 <= 0;
when "00000101" =>  display1 <= 5; display2 <= 9; display3 <= 1; display4 <= 0;
when "00000110" =>  display1 <= 4; display2 <= 3; display3 <= 2; display4 <= 0;
when "00000111" =>  display1 <= 3; display2 <= 7; display3 <= 2; display4 <= 0;
when "00001000" =>  display1 <= 2; display2 <= 1; display3 <= 3; display4 <= 0;
when "00001001" =>  display1 <= 1; display2 <= 5; display3 <= 3; display4 <= 0;
when "00001010" =>  display1 <= 0; display2 <= 9; display3 <= 3; display4 <= 0;
when "00001011" =>  display1 <= 9; display2 <= 2; display3 <= 4; display4 <= 0;
when "00001100" =>  display1 <= 8; display2 <= 6; display3 <= 4; display4 <= 0;
when "00001101" =>  display1 <= 7; display2 <= 0; display3 <= 5; display4 <= 0;
when "00001110" =>  display1 <= 6; display2 <= 4; display3 <= 5; display4 <= 0;
when "00001111" =>  display1 <= 5; display2 <= 8; display3 <= 5; display4 <= 0;
when "00010000" =>  display1 <= 4; display2 <= 2; display3 <= 6; display4 <= 0;
when "00010001" =>  display1 <= 3; display2 <= 6; display3 <= 6; display4 <= 0;
when "00010010" =>  display1 <= 2; display2 <= 0; display3 <= 7; display4 <= 0;
when "00010011" =>  display1 <= 1; display2 <= 4; display3 <= 7; display4 <= 0;
when "00010100" =>  display1 <= 0; display2 <= 8; display3 <= 7; display4 <= 0;
when "00010101" =>  display1 <= 9; display2 <= 1; display3 <= 8; display4 <= 0;
when "00010110" =>  display1 <= 8; display2 <= 5; display3 <= 8; display4 <= 0;
when "00010111" =>  display1 <= 7; display2 <= 9; display3 <= 8; display4 <= 0;
when "00011000" =>  display1 <= 6; display2 <= 3; display3 <= 9; display4 <= 0;
when "00011001" =>  display1 <= 5; display2 <= 7; display3 <= 9; display4 <= 0;
when "00011010" =>  display1 <= 4; display2 <= 1; display3 <= 0; display4 <= 1;
when "00011011" =>  display1 <= 3; display2 <= 5; display3 <= 0; display4 <= 1;
when "00011100" =>  display1 <= 2; display2 <= 9; display3 <= 0; display4 <= 1;
when "00011101" =>  display1 <= 1; display2 <= 3; display3 <= 1; display4 <= 1;
when "00011110" =>  display1 <= 7; display2 <= 7; display3 <= 1; display4 <= 1;
when "00011111" =>  display1 <= 9; display2 <= 0; display3 <= 2; display4 <= 1;
when "00100000" =>  display1 <= 8; display2 <= 4; display3 <= 2; display4 <= 1;
when "00100001" =>  display1 <= 7; display2 <= 8; display3 <= 2; display4 <= 1;
when "00100010" =>  display1 <= 6; display2 <= 2; display3 <= 3; display4 <= 1;
when "00100011" =>  display1 <= 5; display2 <= 6; display3 <= 3; display4 <= 1;
when "00100100" =>  display1 <= 4; display2 <= 0; display3 <= 4; display4 <= 1;
when "00100101" =>  display1 <= 3; display2 <= 4; display3 <= 4; display4 <= 1;
when "00100110" =>  display1 <= 2; display2 <= 8; display3 <= 4; display4 <= 1;
when "00100111" =>  display1 <= 1; display2 <= 2; display3 <= 5; display4 <= 1;
when "00101000" =>  display1 <= 0; display2 <= 6; display3 <= 5; display4 <= 1;
when "00101001" =>  display1 <= 9; display2 <= 9; display3 <= 5; display4 <= 1;
when "00101010" =>  display1 <= 8; display2 <= 3; display3 <= 6; display4 <= 1;
when "00101011" =>  display1 <= 7; display2 <= 7; display3 <= 6; display4 <= 1;
when "00101100" =>  display1 <= 6; display2 <= 1; display3 <= 7; display4 <= 1;
when "00101101" =>  display1 <= 5; display2 <= 5; display3 <= 7; display4 <= 1;
when "00101110" =>  display1 <= 4; display2 <= 9; display3 <= 7; display4 <= 1;
when "00101111" =>  display1 <= 3; display2 <= 3; display3 <= 8; display4 <= 1;
when "00110000" =>  display1 <= 2; display2 <= 7; display3 <= 8; display4 <= 1;
when "00110001" =>  display1 <= 1; display2 <= 1; display3 <= 9; display4 <= 1;
when "00110010" =>  display1 <= 0; display2 <= 5; display3 <= 9; display4 <= 1;
when "00110011" =>  display1 <= 9; display2 <= 8; display3 <= 9; display4 <= 1;
when "00110100" =>  display1 <= 8; display2 <= 2; display3 <= 0; display4 <= 2;
when "00110101" =>  display1 <= 7; display2 <= 6; display3 <= 0; display4 <= 2;
when "00110110" =>  display1 <= 6; display2 <= 0; display3 <= 1; display4 <= 2;
when "00110111" =>  display1 <= 5; display2 <= 4; display3 <= 1; display4 <= 2;
when "00111000" =>  display1 <= 4; display2 <= 8; display3 <= 1; display4 <= 2;
when "00111001" =>  display1 <= 3; display2 <= 2; display3 <= 2; display4 <= 2;
when "00111010" =>  display1 <= 2; display2 <= 6; display3 <= 2; display4 <= 2;
when "00111011" =>  display1 <= 1; display2 <= 0; display3 <= 3; display4 <= 2;
when "00111100" =>  display1 <= 4; display2 <= 4; display3 <= 3; display4 <= 2;
when "00111101" =>  display1 <= 9; display2 <= 7; display3 <= 3; display4 <= 2;
when "00111110" =>  display1 <= 8; display2 <= 1; display3 <= 4; display4 <= 2;
when "00111111" =>  display1 <= 7; display2 <= 5; display3 <= 4; display4 <= 2;
when "01000000" =>  display1 <= 6; display2 <= 9; display3 <= 4; display4 <= 2;
when "01000001" =>  display1 <= 5; display2 <= 3; display3 <= 5; display4 <= 2;
when "01000010" =>  display1 <= 4; display2 <= 7; display3 <= 5; display4 <= 2;
when "01000011" =>  display1 <= 3; display2 <= 1; display3 <= 6; display4 <= 2;
when "01000100" =>  display1 <= 2; display2 <= 5; display3 <= 6; display4 <= 2;
when "01000101" =>  display1 <= 1; display2 <= 9; display3 <= 6; display4 <= 2;
when "01000110" =>  display1 <= 3; display2 <= 3; display3 <= 7; display4 <= 2;
when "01000111" =>  display1 <= 9; display2 <= 6; display3 <= 7; display4 <= 2;
when "01001000" =>  display1 <= 8; display2 <= 0; display3 <= 8; display4 <= 2;
when "01001001" =>  display1 <= 7; display2 <= 4; display3 <= 8; display4 <= 2;
when "01001010" =>  display1 <= 6; display2 <= 8; display3 <= 8; display4 <= 2;
when "01001011" =>  display1 <= 5; display2 <= 2; display3 <= 9; display4 <= 2;
when "01001100" =>  display1 <= 4; display2 <= 6; display3 <= 9; display4 <= 2;
when "01001101" =>  display1 <= 3; display2 <= 0; display3 <= 0; display4 <= 3;
when "01001110" =>  display1 <= 2; display2 <= 4; display3 <= 0; display4 <= 3;
when "01001111" =>  display1 <= 1; display2 <= 8; display3 <= 0; display4 <= 3;
when "01010000" =>  display1 <= 2; display2 <= 2; display3 <= 1; display4 <= 3;
when "01010001" =>  display1 <= 9; display2 <= 5; display3 <= 1; display4 <= 3;
when "01010010" =>  display1 <= 8; display2 <= 9; display3 <= 1; display4 <= 3;
when "01010011" =>  display1 <= 7; display2 <= 3; display3 <= 2; display4 <= 3;
when "01010100" =>  display1 <= 6; display2 <= 7; display3 <= 2; display4 <= 3;
when "01010101" =>  display1 <= 5; display2 <= 1; display3 <= 3; display4 <= 3;
when "01010110" =>  display1 <= 4; display2 <= 5; display3 <= 3; display4 <= 3;
when "01010111" =>  display1 <= 3; display2 <= 9; display3 <= 3; display4 <= 3;
when "01011000" =>  display1 <= 2; display2 <= 3; display3 <= 4; display4 <= 3;
when "01011001" =>  display1 <= 1; display2 <= 7; display3 <= 4; display4 <= 3;
when "01011010" =>  display1 <= 1; display2 <= 1; display3 <= 5; display4 <= 3;
when "01011011" =>  display1 <= 9; display2 <= 4; display3 <= 5; display4 <= 3;
when "01011100" =>  display1 <= 8; display2 <= 8; display3 <= 5; display4 <= 3;
when "01011101" =>  display1 <= 7; display2 <= 2; display3 <= 6; display4 <= 3;
when "01011110" =>  display1 <= 6; display2 <= 6; display3 <= 6; display4 <= 3;
when "01011111" =>  display1 <= 5; display2 <= 0; display3 <= 7; display4 <= 3;
when "01100000" =>  display1 <= 4; display2 <= 4; display3 <= 7; display4 <= 3;
when "01100001" =>  display1 <= 3; display2 <= 8; display3 <= 7; display4 <= 3;
when "01100010" =>  display1 <= 2; display2 <= 2; display3 <= 8; display4 <= 3;
when "01100011" =>  display1 <= 1; display2 <= 6; display3 <= 8; display4 <= 3;
when "01100100" =>  display1 <= 0; display2 <= 0; display3 <= 9; display4 <= 3;
when "01100101" =>  display1 <= 9; display2 <= 3; display3 <= 9; display4 <= 3;
when "01100110" =>  display1 <= 8; display2 <= 7; display3 <= 9; display4 <= 3;
when "01100111" =>  display1 <= 7; display2 <= 1; display3 <= 0; display4 <= 4;
when "01101000" =>  display1 <= 6; display2 <= 5; display3 <= 0; display4 <= 4;
when "01101001" =>  display1 <= 5; display2 <= 9; display3 <= 0; display4 <= 4;
when "01101010" =>  display1 <= 4; display2 <= 3; display3 <= 1; display4 <= 4;
when "01101011" =>  display1 <= 3; display2 <= 7; display3 <= 1; display4 <= 4;
when "01101100" =>  display1 <= 2; display2 <= 1; display3 <= 2; display4 <= 4;
when "01101101" =>  display1 <= 1; display2 <= 5; display3 <= 2; display4 <= 4;
when "01101110" =>  display1 <= 0; display2 <= 9; display3 <= 2; display4 <= 4;
when "01101111" =>  display1 <= 9; display2 <= 2; display3 <= 3; display4 <= 4;
when "01110000" =>  display1 <= 8; display2 <= 6; display3 <= 3; display4 <= 4;
when "01110001" =>  display1 <= 7; display2 <= 0; display3 <= 4; display4 <= 4;
when "01110010" =>  display1 <= 6; display2 <= 4; display3 <= 4; display4 <= 4;
when "01110011" =>  display1 <= 5; display2 <= 8; display3 <= 4; display4 <= 4;
when "01110100" =>  display1 <= 4; display2 <= 2; display3 <= 5; display4 <= 4;
when "01110101" =>  display1 <= 3; display2 <= 6; display3 <= 5; display4 <= 4;
when "01110110" =>  display1 <= 2; display2 <= 0; display3 <= 6; display4 <= 4;
when "01110111" =>  display1 <= 1; display2 <= 4; display3 <= 6; display4 <= 4;
when "01111000" =>  display1 <= 0; display2 <= 8; display3 <= 6; display4 <= 4;
when "01111001" =>  display1 <= 9; display2 <= 1; display3 <= 7; display4 <= 4;
when "01111010" =>  display1 <= 8; display2 <= 5; display3 <= 7; display4 <= 4;
when "01111011" =>  display1 <= 7; display2 <= 9; display3 <= 7; display4 <= 4;
when "01111100" =>  display1 <= 6; display2 <= 3; display3 <= 8; display4 <= 4;
when "01111101" =>  display1 <= 5; display2 <= 7; display3 <= 8; display4 <= 4;
when "01111110" =>  display1 <= 4; display2 <= 1; display3 <= 9; display4 <= 4;
when "01111111" =>  display1 <= 3; display2 <= 5; display3 <= 9; display4 <= 4;
when "10000000" =>  display1 <= 2; display2 <= 9; display3 <= 9; display4 <= 4;
when "10000001" =>  display1 <= 1; display2 <= 3; display3 <= 0; display4 <= 5;
when "10000010" =>  display1 <= 0; display2 <= 7; display3 <= 0; display4 <= 5;
when "10000011" =>  display1 <= 9; display2 <= 0; display3 <= 1; display4 <= 5;
when "10000100" =>  display1 <= 8; display2 <= 4; display3 <= 1; display4 <= 5;
when "10000101" =>  display1 <= 7; display2 <= 8; display3 <= 1; display4 <= 5;
when "10000110" =>  display1 <= 6; display2 <= 2; display3 <= 2; display4 <= 5;
when "10000111" =>  display1 <= 5; display2 <= 6; display3 <= 2; display4 <= 5;
when "10001000" =>  display1 <= 4; display2 <= 0; display3 <= 3; display4 <= 5;
when "10001001" =>  display1 <= 3; display2 <= 4; display3 <= 3; display4 <= 5;
when "10001010" =>  display1 <= 2; display2 <= 8; display3 <= 3; display4 <= 5;
when "10001011" =>  display1 <= 1; display2 <= 2; display3 <= 4; display4 <= 5;
when "10001100" =>  display1 <= 0; display2 <= 6; display3 <= 4; display4 <= 5;
when "10001101" =>  display1 <= 9; display2 <= 9; display3 <= 4; display4 <= 5;
when "10001110" =>  display1 <= 8; display2 <= 3; display3 <= 5; display4 <= 5;
when "10001111" =>  display1 <= 7; display2 <= 7; display3 <= 5; display4 <= 5;
when "10010000" =>  display1 <= 6; display2 <= 1; display3 <= 6; display4 <= 5;
when "10010001" =>  display1 <= 5; display2 <= 5; display3 <= 6; display4 <= 5;
when "10010010" =>  display1 <= 4; display2 <= 9; display3 <= 6; display4 <= 5;
when "10010011" =>  display1 <= 3; display2 <= 3; display3 <= 7; display4 <= 5;
when "10010100" =>  display1 <= 2; display2 <= 7; display3 <= 7; display4 <= 5;
when "10010101" =>  display1 <= 1; display2 <= 1; display3 <= 8; display4 <= 5;
when "10010110" =>  display1 <= 0; display2 <= 5; display3 <= 8; display4 <= 5;
when "10010111" =>  display1 <= 9; display2 <= 8; display3 <= 8; display4 <= 5;
when "10011000" =>  display1 <= 8; display2 <= 2; display3 <= 9; display4 <= 5;
when "10011001" =>  display1 <= 7; display2 <= 6; display3 <= 9; display4 <= 5;
when "10011010" =>  display1 <= 6; display2 <= 0; display3 <= 0; display4 <= 6;
when "10011011" =>  display1 <= 5; display2 <= 4; display3 <= 0; display4 <= 6;
when "10011100" =>  display1 <= 4; display2 <= 8; display3 <= 0; display4 <= 6;
when "10011101" =>  display1 <= 3; display2 <= 2; display3 <= 1; display4 <= 6;
when "10011110" =>  display1 <= 2; display2 <= 6; display3 <= 1; display4 <= 6;
when "10011111" =>  display1 <= 1; display2 <= 0; display3 <= 2; display4 <= 6;
when "10100000" =>  display1 <= 0; display2 <= 4; display3 <= 2; display4 <= 6;
when "10100001" =>  display1 <= 9; display2 <= 7; display3 <= 2; display4 <= 6;
when "10100010" =>  display1 <= 8; display2 <= 1; display3 <= 3; display4 <= 6;
when "10100011" =>  display1 <= 7; display2 <= 5; display3 <= 3; display4 <= 6;
when "10100100" =>  display1 <= 6; display2 <= 9; display3 <= 3; display4 <= 6;
when "10100101" =>  display1 <= 5; display2 <= 3; display3 <= 4; display4 <= 6;
when "10100110" =>  display1 <= 4; display2 <= 7; display3 <= 4; display4 <= 6;
when "10100111" =>  display1 <= 3; display2 <= 1; display3 <= 5; display4 <= 6;
when "10101000" =>  display1 <= 2; display2 <= 5; display3 <= 5; display4 <= 6;
when "10101001" =>  display1 <= 1; display2 <= 9; display3 <= 5; display4 <= 6;
when "10101010" =>  display1 <= 0; display2 <= 3; display3 <= 6; display4 <= 6;
when "10101011" =>  display1 <= 9; display2 <= 6; display3 <= 6; display4 <= 6;
when "10101100" =>  display1 <= 8; display2 <= 0; display3 <= 7; display4 <= 6;
when "10101101" =>  display1 <= 7; display2 <= 4; display3 <= 7; display4 <= 6;
when "10101110" =>  display1 <= 6; display2 <= 8; display3 <= 7; display4 <= 6;
when "10101111" =>  display1 <= 5; display2 <= 2; display3 <= 8; display4 <= 6;
when "10110000" =>  display1 <= 4; display2 <= 6; display3 <= 8; display4 <= 6;
when "10110001" =>  display1 <= 3; display2 <= 0; display3 <= 9; display4 <= 6;
when "10110010" =>  display1 <= 2; display2 <= 4; display3 <= 9; display4 <= 6;
when "10110011" =>  display1 <= 1; display2 <= 8; display3 <= 9; display4 <= 6;
when "10110100" =>  display1 <= 0; display2 <= 2; display3 <= 0; display4 <= 7;
when "10110101" =>  display1 <= 9; display2 <= 5; display3 <= 0; display4 <= 7;
when "10110110" =>  display1 <= 8; display2 <= 9; display3 <= 0; display4 <= 7;
when "10110111" =>  display1 <= 7; display2 <= 3; display3 <= 1; display4 <= 7;
when "10111000" =>  display1 <= 6; display2 <= 7; display3 <= 1; display4 <= 7;
when "10111001" =>  display1 <= 5; display2 <= 1; display3 <= 2; display4 <= 7;
when "10111010" =>  display1 <= 4; display2 <= 5; display3 <= 2; display4 <= 7;
when "10111011" =>  display1 <= 3; display2 <= 9; display3 <= 2; display4 <= 7;
when "10111100" =>  display1 <= 2; display2 <= 3; display3 <= 3; display4 <= 7;
when "10111101" =>  display1 <= 1; display2 <= 7; display3 <= 3; display4 <= 7;
when "10111110" =>  display1 <= 0; display2 <= 1; display3 <= 4; display4 <= 7;
when "10111111" =>  display1 <= 9; display2 <= 4; display3 <= 4; display4 <= 7;
when "11000000" =>  display1 <= 8; display2 <= 8; display3 <= 4; display4 <= 7;
when "11000001" =>  display1 <= 7; display2 <= 2; display3 <= 5; display4 <= 7;
when "11000010" =>  display1 <= 6; display2 <= 6; display3 <= 5; display4 <= 7;
when "11000011" =>  display1 <= 5; display2 <= 0; display3 <= 6; display4 <= 7;
when "11000100" =>  display1 <= 4; display2 <= 4; display3 <= 6; display4 <= 7;
when "11000101" =>  display1 <= 3; display2 <= 8; display3 <= 6; display4 <= 7;
when "11000110" =>  display1 <= 2; display2 <= 2; display3 <= 7; display4 <= 7;
when "11000111" =>  display1 <= 1; display2 <= 6; display3 <= 7; display4 <= 7;
when "11001000" =>  display1 <= 0; display2 <= 0; display3 <= 8; display4 <= 7;
when "11001001" =>  display1 <= 9; display2 <= 3; display3 <= 8; display4 <= 7;
when "11001010" =>  display1 <= 8; display2 <= 7; display3 <= 8; display4 <= 7;
when "11001011" =>  display1 <= 7; display2 <= 1; display3 <= 9; display4 <= 7;
when "11001100" =>  display1 <= 6; display2 <= 5; display3 <= 9; display4 <= 7;
when "11001101" =>  display1 <= 5; display2 <= 9; display3 <= 9; display4 <= 7;
when "11001110" =>  display1 <= 4; display2 <= 3; display3 <= 0; display4 <= 8;
when "11001111" =>  display1 <= 3; display2 <= 7; display3 <= 0; display4 <= 8;
when "11010000" =>  display1 <= 2; display2 <= 1; display3 <= 1; display4 <= 8;
when "11010001" =>  display1 <= 1; display2 <= 5; display3 <= 1; display4 <= 8;
when "11010010" =>  display1 <= 0; display2 <= 9; display3 <= 1; display4 <= 8;
when "11010011" =>  display1 <= 9; display2 <= 2; display3 <= 2; display4 <= 8;
when "11010100" =>  display1 <= 8; display2 <= 6; display3 <= 2; display4 <= 8;
when "11010101" =>  display1 <= 7; display2 <= 0; display3 <= 3; display4 <= 8;
when "11010110" =>  display1 <= 6; display2 <= 4; display3 <= 3; display4 <= 8;
when "11010111" =>  display1 <= 5; display2 <= 8; display3 <= 3; display4 <= 8;
when "11011000" =>  display1 <= 4; display2 <= 2; display3 <= 4; display4 <= 8;
when "11011001" =>  display1 <= 3; display2 <= 6; display3 <= 4; display4 <= 8;
when "11011010" =>  display1 <= 2; display2 <= 0; display3 <= 5; display4 <= 8;
when "11011011" =>  display1 <= 1; display2 <= 4; display3 <= 5; display4 <= 8;
when "11011100" =>  display1 <= 0; display2 <= 8; display3 <= 5; display4 <= 8;
when "11011101" =>  display1 <= 9; display2 <= 1; display3 <= 6; display4 <= 8;
when "11011110" =>  display1 <= 8; display2 <= 5; display3 <= 6; display4 <= 8;
when "11011111" =>  display1 <= 7; display2 <= 9; display3 <= 6; display4 <= 8;
when "11100000" =>  display1 <= 6; display2 <= 3; display3 <= 7; display4 <= 8;
when "11100001" =>  display1 <= 5; display2 <= 7; display3 <= 7; display4 <= 8;
when "11100010" =>  display1 <= 4; display2 <= 1; display3 <= 8; display4 <= 8;
when "11100011" =>  display1 <= 3; display2 <= 5; display3 <= 8; display4 <= 8;
when "11100100" =>  display1 <= 2; display2 <= 9; display3 <= 8; display4 <= 8;
when "11100101" =>  display1 <= 1; display2 <= 3; display3 <= 9; display4 <= 8;
when "11100110" =>  display1 <= 0; display2 <= 7; display3 <= 9; display4 <= 8;
when "11100111" =>  display1 <= 9; display2 <= 0; display3 <= 0; display4 <= 9;
when "11101000" =>  display1 <= 8; display2 <= 4; display3 <= 0; display4 <= 9;
when "11101001" =>  display1 <= 7; display2 <= 8; display3 <= 0; display4 <= 9;
when "11101010" =>  display1 <= 6; display2 <= 2; display3 <= 1; display4 <= 9;
when "11101011" =>  display1 <= 5; display2 <= 6; display3 <= 1; display4 <= 9;
when "11101100" =>  display1 <= 4; display2 <= 0; display3 <= 2; display4 <= 9;
when "11101101" =>  display1 <= 3; display2 <= 4; display3 <= 2; display4 <= 9;
when "11101110" =>  display1 <= 2; display2 <= 8; display3 <= 2; display4 <= 9;
when "11101111" =>  display1 <= 1; display2 <= 2; display3 <= 3; display4 <= 9;
when "11110000" =>  display1 <= 0; display2 <= 6; display3 <= 3; display4 <= 9;
when "11110001" =>  display1 <= 9; display2 <= 9; display3 <= 3; display4 <= 9;
when "11110010" =>  display1 <= 8; display2 <= 3; display3 <= 4; display4 <= 9;
when "11110011" =>  display1 <= 7; display2 <= 7; display3 <= 4; display4 <= 9;
when "11110100" =>  display1 <= 6; display2 <= 1; display3 <= 5; display4 <= 9;
when "11110101" =>  display1 <= 5; display2 <= 5; display3 <= 5; display4 <= 9;
when "11110110" =>  display1 <= 4; display2 <= 9; display3 <= 5; display4 <= 9;
when "11110111" =>  display1 <= 3; display2 <= 3; display3 <= 6; display4 <= 9;
when "11111000" =>  display1 <= 2; display2 <= 7; display3 <= 6; display4 <= 9;
when "11111001" =>  display1 <= 1; display2 <= 1; display3 <= 7; display4 <= 9;
when "11111010" =>  display1 <= 0; display2 <= 5; display3 <= 7; display4 <= 9;
when "11111011" =>  display1 <= 9; display2 <= 8; display3 <= 7; display4 <= 9;
when "11111100" =>  display1 <= 8; display2 <= 2; display3 <= 8; display4 <= 9;
when "11111101" =>  display1 <= 7; display2 <= 6; display3 <= 8; display4 <= 9;
when "11111110" =>  display1 <= 6; display2 <= 0; display3 <= 9; display4 <= 9;
when "11111111" =>  display1 <= 5; display2 <= 4; display3 <= 9; display4 <= 9;

	end case;
end process;

aumenta: process(mais,rst) -- processo utilizado para aumentar o pwm_in por tecla
begin
if (rst = '0' ) then -- reset 
	auxt<="00000000"; 
elsif falling_edge(mais) then -- quando a tecla e' pressionada (borda de descida)
	if (auxt <= "11111111") then  	  
		auxt<=auxt + '1';       -- e' somado '1' ao valor anterior 
	end if;
end if;
end process;

diminui: process(menos,rst)
begin
if (rst = '0' ) then  
	auxm<="00000000";
elsif falling_edge(menos) then
	if (auxm >= "00000000") then	
		auxm<=auxm + '1';       -- e' somado '1' ao valor anterior 
	end if;							-- na logica final é uma subtracao
end if;	
end process;

processo: process(auxc,clk,rst) -- processo dependente do clock 
begin
if (rst = '0' ) then
	auxc<="00000000";
else if rising_edge(clk) then    -- borda de subida do clock
	if (rampa_c_set='1')  then    -- compara se esta em mdo automatico
		if (rampa_mais_menos_set='1') then  -- seleciona rampa crescente ou decrescente
			if (contador=500000) then   -- compara se o valor do contador e' o maximo
				auxc<= (auxc + "00000001");  -- soma '1' ao valor anterior
				contador<=0;                 -- quando o contador chega ao maxima e' zerado
			else
				contador <= contador+1;   --caso o contador nao esteja no maximo soma '1'
			end if;
		else -- seleciona rampa crescente ou decrescente
			if (contador=500000) then   -- compara se o valor do contador e' o maximo
				auxc<= (auxc - "00000001"); -- subtrai '1' ao valor anterior
				contador<=0; -- quando o contador chega ao maxima e' zerado
			else
				contador <= contador+1; --caso o contador nao esteja no maximo soma '1'
			end if;
	end if;
	end if;
end if;
end if;
end process;

processo2: process(clk,rst) -- processo do codificador Sigma-Delta
begin
PWM_in<=auxt-auxm+auxc;  -- logica para atribuir o valor da entrada do codificador
if rst ='0' then  -- reset (zera todas variaveis)
	ddc<="00000000";
	pwm_in<="00000000";
	pwm_Somador<="000000000";
elsif pwm_in="11111111" then -- comparacao para fixar o valor no máximo
	pwm_Somador<="111111111";
else 
	if rising_edge(clk) then  -- evento da borda de subida do clock
		if pwm_somador(8)='1' then  -- compara o nono bit da saida do codificador
			ddc<= "11111111";  -- caso a saida seja '1' o conversor DDC recebe '0xFF'
		else
			ddc<= "00000000";  -- caso seja '0', o DDC recebe '0x00'
		end if;
			diff <= ("0" & pwm_in) - ( "0" & ddc); -- executa a operaçao de diferença (DELTA)
			PWM_Somador  <=  ("0" & PWM_Somador(7 downto 0)) + ("0" & diff(7 downto 0)) ; 
			-- a concatenaçao e soma desses sinais funciona como um acumulador (SIGMA)
	end if;
end if;

end process;

  PWM_out <= PWM_Somador(8); -- atribui o sinal (alto ou baixo) ao pino de saida
  pwm_test<= pwm_in; -- pinos de teste utilizados para verificacao do sinal de entrada
 
 
--Atribuicao dos valores da tabela ao display de 7 segmentos 
with display1 select
    displayA <= 
	  "1000000" when 0, -- '0'
     "1111001" when 1, -- '1'
     "0100100" when 2, -- '2'	 
     "0110000" when 3, -- '3'	 
     "0011001" when 4, -- '4'
     "0010010" when 5, -- '5'
	  "0000011" when 6, -- '6'
	  "1111000" when 7, -- '7'
	  "0000000" when 8, -- '8'
	  "0011000" when 9, -- '9'
	  "1111111" when others;

with display2 select
    displayB <= 
	  "1000000" when 0, -- '0'
     "1111001" when 1, -- '1'
     "0100100" when 2, -- '2'	 
     "0110000" when 3, -- '3'	 
     "0011001" when 4, -- '4'
     "0010010" when 5, -- '5'
	  "0000011" when 6, -- '6'
	  "1111000" when 7, -- '7'
	  "0000000" when 8, -- '8'
	  "0011000" when 9, -- '9'
	  "1111111" when others;

with display3 select
    displayC <= 
	  "1000000" when 0, -- '0'
     "1111001" when 1, -- '1'
     "0100100" when 2, -- '2'	 
     "0110000" when 3, -- '3'	 
     "0011001" when 4, -- '4'
     "0010010" when 5, -- '5'
	  "0000011" when 6, -- '6'
	  "1111000" when 7, -- '7'
	  "0000000" when 8, -- '8'
	  "0011000" when 9, -- '9'
	  "1111111" when others;

with display4 select
    displayD <= 
	  "1000000" when 0, -- '0'
     "1111001" when 1, -- '1'
     "0100100" when 2, -- '2'	 
     "0110000" when 3, -- '3'	 
     "0011001" when 4, -- '4'
     "0010010" when 5, -- '5'
	  "0000011" when 6, -- '6'
	  "1111000" when 7, -- '7'
	  "0000000" when 8, -- '8'
	  "0011000" when 9, -- '9'
	  "1111111" when others;	  
end PWM_funcionamento;

------